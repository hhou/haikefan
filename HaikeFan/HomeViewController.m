//
//  FirstViewController.m
//  HaikeFan
//
//  Created by Henry Hou on 4/1/15.
//  Copyright (c) 2015 Airdataservices. All rights reserved.
//

#import "HomeViewController.h"

@interface HomeViewController ()

@end

@implementation HomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    
    self.tabBarItem.image = [[UIImage imageNamed:@"home"]
                             imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
