//
//  HomeViewController.m
//  HaikeFan
//
//  Created by Henry Hou on 4/1/15.
//  Copyright (c) 2015 Airdataservices. All rights reserved.
//

#import "MainNavViewController.h"
#import "MenuScrollView.h"

@interface MainNavViewController ()

@end

@implementation MainNavViewController{
    
    __weak IBOutlet MenuScrollView *_menuScrollView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (IBAction)showhideMenu:(id)sender {
    if(_menuScrollView.frame.origin.y == 64){
        [UIView animateWithDuration:0.8 delay:0.0 options:0
                         animations:^{
                             _menuScrollView.frame = CGRectMake(0,
                                                                64 - _menuScrollView.frame.size.height,
                                                                _menuScrollView.frame.size.width, _menuScrollView.frame.size.height);
                         }
                         completion:nil];
    }else if(_menuScrollView.frame.origin.y == 64 - _menuScrollView.frame.size.height){
        [UIView animateWithDuration:.8
                              delay:0.0
             usingSpringWithDamping:0.5
              initialSpringVelocity:0.5
                            options:UIViewAnimationOptionTransitionNone animations:^{
                                _menuScrollView.frame = CGRectMake(0,
                                                                   64 ,
                                                                   _menuScrollView.frame.size.width, _menuScrollView.frame.size.height);
                            }
                         completion:^(BOOL finished) {
                             //Completion Block
                         }];
    }
}

@end
