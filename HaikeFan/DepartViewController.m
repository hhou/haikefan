//
//  SecondViewController.m
//  HaikeFan
//
//  Created by Henry Hou on 4/1/15.
//  Copyright (c) 2015 Airdataservices. All rights reserved.
//

#import "DepartViewController.h"

@interface DepartViewController ()

@end

@implementation DepartViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    self.tabBarItem.image = [[UIImage imageNamed:@"chufa"]
                             imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
