//
//  AppDelegate.h
//  HaikeFan
//
//  Created by Henry Hou on 4/1/15.
//  Copyright (c) 2015 Airdataservices. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

